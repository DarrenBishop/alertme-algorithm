package com.darrenbishop.alerme;

/**
 * Hello world!
 *
 */
public class ArrayFinder
{
    public static int find(int[] values, int[] search) {

        for (int i = 0; i < values.length; i++) {

            if (values[i] == search[0]) {

                if (i + search.length > values.length) {
                    break;
                }

                boolean found = false;

                for (int j = 1; j < search.length && i+j < values.length; j++) {

                    found = values[i+j] == search[j];
                }

                if (found)
                    return i;
            }
        }
        return -1;
    }
}
