package com.darrenbishop.alerme;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;

/**
 * Implement a class that given two arrays, will find the starting position of the second array in the first array. i.e.
 *
 *     [2,3,4,5] and [4,5] should return 2.
 *
 * Explain your solution. Write some tests for this.
 */
public class ArrayFinderTest {

    /**
     * [2,3,4,5] and [4,5] should return 2.
     */
    @Test
    public void findArrayAtTheEnd() {

        int[] values = {2, 3, 4, 5};
        int[] search = {4, 5};

        assertThat(ArrayFinder.find(values, search), is(equalTo(2)));
    }

    /**
     * [2,3,4,5] and [1,5] should return -1.
     */
    @Test
    public void arrayNotFound() {

        int[] values = {2, 3, 4, 5};
        int[] search = {1, 5};

        assertThat(ArrayFinder.find(values, search), is(equalTo(-1)));
    }

    /**
     * [2,3,4,5] and [4,5,6] should return -1.
     */
    @Test
    public void searchArrayOverflows() {

        int[] values = {2, 3, 4, 5};
        int[] search = {4, 5, 6};

        assertThat(ArrayFinder.find(values, search), is(equalTo(-1)));
    }
}
